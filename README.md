# Hookpulse

Hookpulse is an innovative webhook management platform designed to offer a simpliﬁed and centralised approach
approach to data ﬂux management.

## Installation and start-up

To make it easy for you to install project, here's a list of recommended next steps.

1. Clone the project from the Git repository
2. Go to the project directory
3. Check the availability of your ports, in particular
    - port 8084 on which the project (Hookpulse) starts
    - port 5432 on which the PostgreSql DBMS starts up
    - port 8083 on which the GUI for adminer starts up
    - port 25 on which the SMTP server for sending mail locally starts up
    - port 5000 for the SMTP GUI
4. Navigate to the project resources folder with the command :
    ```
    cd src/main/resources
    ```
5. Launch the Docker containers using Docker Compose (the -d option allows you to run in the background):
     ```
   docker compose up -d
   ```    
6. Launch / Build the project by clicking build icon on IDE

## Start-up preview

To visualise, you can:

1. Visualise the GUI Admuner on your browser by enter:
    ```
    http://localhost:8083/
   ``` 
2. Visualise the GUI SMTP on your browser by enter:
    ```
    http://localhost:5000/
   ```
3. Register a new user by sending a json format like the following to the following link.
    ```
    {
       "firstname": "Roland",
       "lastname": "KUATE",
       "email": "rolandmartialkuate@gmail.com",
       "password": "1111111111"
   }
   ```
    ```
    http://localhost:8084/hook-pulse/api/register
   ```

4. Use some client like Postman to send request to our project api 
by completing the follow link with a specific action eg:webhooks :
    ```
    http://localhost:8084/hook-pulse/api/webhooks
   ```

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
