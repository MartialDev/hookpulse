package chillo.tech.hookpulse

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HookPulseApplication

fun main(args: Array<String>) {
	runApplication<HookPulseApplication>(*args)
}
