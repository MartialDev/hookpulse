package chillo.tech.hookpulse.controller

import chillo.tech.hookpulse.entity.User
import chillo.tech.hookpulse.service.UserService
import lombok.AllArgsConstructor
import lombok.extern.slf4j.Slf4j
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
class UserController(private val userService: UserService) {

    @PostMapping(path = ["register"])
    fun register(@RequestBody user: User) {
        this.userService.register(user)
    }

    @PostMapping(path = ["activate"])
    fun activate(@RequestBody code: Map<String, String>) {
        this.userService.activate(code)
    }
}
