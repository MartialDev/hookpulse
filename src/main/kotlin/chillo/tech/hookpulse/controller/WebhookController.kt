package chillo.tech.hookpulse.controller

import chillo.tech.hookpulse.entity.Webhook
import chillo.tech.hookpulse.service.WebhookService
import lombok.AllArgsConstructor
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@AllArgsConstructor
@RequestMapping("webhooks")
@RestController
class WebhookController {
    private val webhookService: WebhookService? = null

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    fun create(@RequestBody webhook: Webhook?) {
        if (webhook != null) {
            webhookService?.createWebhook(webhook)
        }
    }
}