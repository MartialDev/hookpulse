package chillo.tech.hookpulse.entity

import chillo.tech.hookpulse.utils.enums.RoleType
import jakarta.persistence.*

/**
 * Entity Role to store user's role
 *
 * @constructor Create empty Role
 * @property id
 * @property label
 */
@Entity
@Table(name = "roles")
class Role(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private var id: Int,
    @Enumerated(EnumType.STRING)
    private var label: RoleType
) {

    fun getId(): Int {
        return this.id
    }

    fun getLabel(): RoleType {
        return this.label
    }

    fun setLabel(label: RoleType){
        this.label = label
    }
}