package chillo.tech.hookpulse.entity

import jakarta.persistence.*
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

/**
 * Entity User to store users
 *
 * @constructor Create empty User
 * @property id
 * @property password
 * @property email
 * @property firstname
 * @property lastname
 * @property active
 * @property role
 */
@Entity
@Table(name = "users")
class User(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private var id: Int,
    private var password: String,
    private var email: String,
    private var firstname: String,
    private var lastname: String,
    private var active: Boolean = false,
    @OneToOne(cascade = [CascadeType.ALL])
    private var role: Role?
) : UserDetails {


    fun getId(): Int {
        return this.id
    }

    fun getFirstname(): String {
        return this.firstname
    }

    fun setFirstname(firstname: String){
        this.firstname = firstname
    }

    fun getLastname(): String {
        return this.lastname
    }

    fun setLastname(lastname: String){
        this.lastname = lastname
    }

    fun getRole(): Role? {
        return this.role
    }

    fun setRole(role: Role?){
        this.role = role
    }

    fun getEmail(): String{
        return this.email
    }

    fun setPassword(password: String){
        this.password = password
    }

    fun setActive(active: Boolean){
        this.active = active
    }

    override fun getAuthorities(): Collection<GrantedAuthority?> {
        return listOf(SimpleGrantedAuthority("ROLE_" + role?.getLabel()))
    }

    override fun getPassword(): String {
        return password
    }

    override fun getUsername(): String {
        return email
    }

    override fun isAccountNonExpired(): Boolean {
        return this.active
    }

    override fun isAccountNonLocked(): Boolean {
        return this.active
    }

    override fun isCredentialsNonExpired(): Boolean {
        return this.active
    }

    override fun isEnabled(): Boolean {
        return this.active
    }
}