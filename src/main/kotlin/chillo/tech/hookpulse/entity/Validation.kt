package chillo.tech.hookpulse.entity

import jakarta.persistence.*
import java.time.Instant

/**
 * Entity Validation to store informations about user validation registration
 *
 * @constructor Create empty Validation
 * @property id
 * @property createAt
 * @property expireAt
 * @property activateAt
 * @property code
 * @property user
 */
@Entity
@Table(name = "validations")
class Validation(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private var id: Int,
    private var createAt: Instant,
    private var expireAt: Instant,
    private var code: String,
    @OneToOne
    private var user: User
) {
    private lateinit var activateAt: Instant

    fun getId(): Int {
        return this.id
    }

    fun getUser(): User {
        return this.user
    }

    fun setUser(user: User){
        this.user = user
    }

    fun getCode(): String {
        return this.code
    }

    fun setCode(code: String){
        this.code = code
    }

    fun getCreateAt(): Instant {
        return this.createAt
    }

    fun setCreateAt(createAt: Instant){
        this.createAt = createAt
    }

    fun getExpireAt(): Instant {
        return this.expireAt
    }

    fun setExpireAt(expireAt: Instant){
        this.expireAt = expireAt
    }

    fun getActivateAt(): Instant {
        return this.activateAt
    }

    fun setActivateAt(activateAt: Instant){
        this.activateAt = activateAt
    }
}