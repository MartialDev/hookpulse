package chillo.tech.hookpulse.entity

import jakarta.persistence.*

/**
 * Entity Webhook to store customer webhook eg: stripe, tally, whatsapp etc...
 *
 * @constructor Create empty Webhook
 * @property id
 * @property url
 * @property description
 * @property active
 */
@Entity
@Table(name = "webhooks")
class Webhook(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private var id: Int,
    private var url: String,
    private var description: String,
    private var active: Boolean = false
) {

    fun getId(): Int {
        return this.id
    }
}