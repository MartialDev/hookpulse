package chillo.tech.hookpulse.repository

import chillo.tech.hookpulse.entity.Role
import org.springframework.data.repository.CrudRepository
import java.util.Optional

interface RoleRepository : CrudRepository<Role, Int>{

    fun findRoleByLabel(label: String): Optional<Role>
}