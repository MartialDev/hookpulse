package chillo.tech.hookpulse.repository

import chillo.tech.hookpulse.entity.Validation
import org.springframework.data.repository.CrudRepository
import java.util.*

interface ValidationRepository: CrudRepository<Validation, Int>{
    fun findByCode(code: String): Optional<Validation>
}