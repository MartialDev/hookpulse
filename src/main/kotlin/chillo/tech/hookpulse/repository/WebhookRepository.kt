package chillo.tech.hookpulse.repository

import chillo.tech.hookpulse.entity.Webhook
import org.springframework.data.repository.CrudRepository

interface WebhookRepository : CrudRepository<Webhook, Int>