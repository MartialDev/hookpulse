package chillo.tech.hookpulse.service

import chillo.tech.hookpulse.entity.Validation
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service

@Service
class NotificationService(
    private var javaMailSender: JavaMailSender
) {
    fun sendNotification(validation: Validation){
        val message: SimpleMailMessage = SimpleMailMessage()
        message.from = "no-reply@chillo.tech"
        message.setTo(validation.getUser().getEmail())
        message.subject = "Activation code"

        val text = String.format(
            "Hello %s, <br /> Your Activation code to confirm registration is %s <br /> See you soon!",
            validation.getUser().getFirstname(),
            validation.getCode()
        )
        message.text = text

        javaMailSender.send(message)
    }
}