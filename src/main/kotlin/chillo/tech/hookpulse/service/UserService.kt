package chillo.tech.hookpulse.service

import chillo.tech.hookpulse.entity.User
import chillo.tech.hookpulse.repository.UserRepository
import lombok.AllArgsConstructor
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import chillo.tech.hookpulse.utils.functions.isEmailValid
import chillo.tech.hookpulse.utils.functions.isNameValid
import java.time.Instant
import java.util.Optional

@Service
@AllArgsConstructor
class UserService(
    private val userRepository: UserRepository,
    private val validationService: ValidationService,
    private val passwordEncoder: BCryptPasswordEncoder
) {

    fun register(user: User) {
        if (!isEmailValid(user.getEmail())){
            throw RuntimeException("Invalid email")
        }
        if (!isNameValid(user.getLastname())){
            throw RuntimeException("Invalid Lastname")
        }
        if (!isNameValid(user.getFirstname())){
            throw RuntimeException("Invalid Firstname")
        }

        val optionalUser: Optional<User> = this.userRepository.findByEmail(user.getEmail())
        if(optionalUser.isPresent){
            throw RuntimeException("User with current email already exist")
        }

        val password = this.passwordEncoder.encode(user.password)
        user.setPassword(password)

        val user = this.userRepository.save(user)
        this.validationService.createValidation(user)
    }

    fun activate(code: Map<String, String>) {
        val validation = this.validationService.getValidation(code.getValue("code"))
        if (Instant.now().isAfter(validation.getExpireAt())){
            throw RuntimeException("Expired code")
        }
        val userActivated = this.userRepository.findById(validation.getUser().getId()).orElseThrow { RuntimeException("User not exist") }
        userActivated.setActive(true)
        this.userRepository.save(userActivated)
    }


}