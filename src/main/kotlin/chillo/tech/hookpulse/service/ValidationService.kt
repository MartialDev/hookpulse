package chillo.tech.hookpulse.service

import chillo.tech.hookpulse.entity.User
import chillo.tech.hookpulse.entity.Validation
import chillo.tech.hookpulse.repository.ValidationRepository
import lombok.AllArgsConstructor
import org.springframework.stereotype.Service
import java.time.Instant
import java.util.*

@AllArgsConstructor
@Service
class ValidationService(
    private val validationRepository: ValidationRepository,
    private val notificationService: NotificationService
) {

    fun createValidation(user: User) {
        val createAt = Instant.now()
        val expireAt = createAt.plusSeconds(36000)
        val random: Random = Random()
        val randomInteger = random.nextInt(999999)
        val code: String = String.format("%06d", randomInteger)
        val validation: Validation = Validation(0, createAt, expireAt, code, user)

        validationRepository.save(validation)
        this.notificationService.sendNotification(validation)
    }

    fun getValidation(code: String): Validation {
        return this.validationRepository.findByCode(code).orElseThrow { RuntimeException("Invalid Code") }
    }
}