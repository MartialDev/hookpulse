package chillo.tech.hookpulse.service

import chillo.tech.hookpulse.entity.Webhook
import chillo.tech.hookpulse.repository.WebhookRepository
import lombok.AllArgsConstructor
import org.springframework.stereotype.Service

@AllArgsConstructor
@Service
class WebhookService(private val webhookRepository: WebhookRepository) {

    fun createWebhook(webhook: Webhook) {
        webhookRepository.save(webhook)
    }
}