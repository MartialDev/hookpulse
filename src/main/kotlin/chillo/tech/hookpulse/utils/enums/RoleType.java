package chillo.tech.hookpulse.utils.enums;

public enum RoleType {
    USER,
    ADMINISTRATOR
}
